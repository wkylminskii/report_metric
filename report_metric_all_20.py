import telegram
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sns
import io
import pandas as pd
import pandahouse
from read_db.CH import Getch
import os

sns.set()


def test_report(chat=None):
    chat_id = -708816698
    bot = telegram.Bot(token=os.environ.get('BOT_TOKEN')) # чтобы не показывать token
    
    data_active_user = Getch(
    '''
    select 
        toDate(simulator.feed_actions.time) as day,
        count(distinct simulator.feed_actions.user_id) as count_unique
    from simulator.feed_actions
    inner join simulator.message_actions on simulator.feed_actions.user_id = simulator.message_actions.user_id
    where toDate(time) = today() - 1
    group by toDate(simulator.feed_actions.time)
    '''
    ).df
    
    data_like_view = Getch(
    '''
    select 
        toDate(time) as day,
        countIf(user_id, action='like') as count_like,
        countIf(user_id, action='view') as count_view
    from simulator.feed_actions
    where toStartOfDay(time) = today() - 1
    group by toDate(time)
    '''
    ).df
    
    data_message = Getch(
    '''
    select 
        toDate(time) as day, 
        count(user_id) as send_message
    from simulator.message_actions
    where toStartOfDay(time) = today() - 1
    group by toDate(time)
    '''
    ).df
    
    
    msg = f'Единый отчет за {data_active_user.day.to_string(index=False)}:\n\
    active_user = {round(int(data_active_user.count_unique.to_string(index=False))/1000)}K\n\
    view = {round(int(data_like_view.count_view.to_string(index=False))/1000)}K\n\
    like = {round(int(data_like_view.count_like.to_string(index=False))/1000)}K\n\
    message = {round(int(data_message.send_message.to_string(index=False))/1000)}K'

    bot.sendMessage(chat_id=chat_id, text=msg)
    
    
    data_active_user_7 = Getch(
    '''
    select 
        toDate(simulator.feed_actions.time) as day,
        count(distinct simulator.feed_actions.user_id) as count_unique
    from simulator.feed_actions
    inner join simulator.message_actions on simulator.feed_actions.user_id = simulator.message_actions.user_id
    where toStartOfDay(time) > today() - 8 and toStartOfDay(time) < today()
    group by toDate(time)
    '''
    ).df
    
    data_like_view_7 = Getch(
    '''
    select 
        toDate(time) as day,
        countIf(user_id, action='like') as count_like,
        countIf(user_id, action='view') as count_view
    from simulator.feed_actions
    where toStartOfDay(time) > today() - 8 and toStartOfDay(time) < today()
    group by toDate(time)
    '''
    ).df
    
    data_message_7 = Getch(
    '''
    select 
        toDate(time) as day, 
        count(user_id) as send_message
    from simulator.message_actions
    where toStartOfDay(time) > today() - 8 and toStartOfDay(time) < today()
    group by toDate(time)
    '''
    ).df
    
    
    metric_df = data_active_user_7.merge(data_like_view_7, on='day').merge(data_message_7, on='day')
    
    
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(18, 16))
    sns.lineplot(data=metric_df, x='day', y='count_unique', ax=ax[0][0])
    sns.lineplot(data=metric_df, x='day', y='count_like', ax=ax[0][1])
    sns.lineplot(data=metric_df, x='day', y='count_view', ax=ax[1][0])
    sns.lineplot(data=metric_df, x='day', y='send_message', ax=ax[1][1])

    myFmt = mdates.DateFormatter('%d-%b')
    ax[0][0].xaxis.set_major_formatter(myFmt)
    ax[0][1].xaxis.set_major_formatter(myFmt)
    ax[1][0].xaxis.set_major_formatter(myFmt)
    ax[1][1].xaxis.set_major_formatter(myFmt)

    ax[0][0].grid(visible=True)
    ax[0][1].grid(visible=True)
    ax[1][0].grid(visible=True)
    ax[1][1].grid(visible=True)

    ax[0][0].set_title('Количество уникальных пользователей обоих сервисов\n')
    ax[0][1].set_title('Количество лайков\n')
    ax[1][0].set_title('Количество просмотров\n')
    ax[1][1].set_title('Количество отправленных сообщений\n');

    import io # позволяет перенаправлять потоки ввода вывода данных, тем самым мы сохраним график не как файл в вайловой системе,
    # а как файловый объект в буфере и соответственно будем доставать его из буфера

    # зададим файловый объект
    plot_object = io.BytesIO()
    # сохраним в него наш график
    plt.savefig(plot_object)
    # зададим имя нашему файловому объекту
    plot_object.name = 'metric_plot.png'
    # перенесем курсор из конца файлового объекта в начало, чтобы прочитать весь файл
    plot_object.seek(0)
    # закроем файл
    plt.close()
    # отправим изображение
    bot.sendPhoto(chat_id=chat_id, photo=plot_object)
    

try:
    test_report()
except Exception as e:
    print(e)
   